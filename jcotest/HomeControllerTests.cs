using Xunit; // for xUnit assertions
using Microsoft.AspNetCore.Mvc; // for IActionResult, ViewResult
using Moq;
using cicdprj.Controllers;
using Microsoft.Extensions.Logging; // for mocking dependencies (optional)

namespace jcotest
{
    public class HomeControllerTests
    {
        private readonly HomeController _controller;
        private readonly ILogger<HomeController> _logger;

        public HomeControllerTests()
        {
       
            _controller = new HomeController(_logger);
        }

        [Fact]
        public void Error_ReturnsViewWithRequestId()
        {
            // Arrange (setup)
            var result = _controller.Index();
            Assert.IsType<ViewResult>(result);
        }
    }
}